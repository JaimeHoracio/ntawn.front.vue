/* eslint-disable prefer-const */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-trailing-spaces */
/* eslint-disable no-const-assign */
/* eslint-disable no-undef */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import moment from 'moment'

Vue.use(Vuex)

export async function consutlarServer (url) {
  // const url = 'https://localhost:44323/api/Login?User='.concat(payload.user).concat('&Pass=').concat(payload.pass)
      
  console.log('URL:' + url)

  await axios
    .get(url)
    .then((result) => {
      console.log(result.data)
      return result.data
    })
    .catch(error => {
      console.log('>>>>Ocurrio un error dentro del axios:' + error)
    })
}

export default new Vuex.Store({
  state: {
    userLogued : null,
    logueded: false,
    register: false,
    detailBanda:{},
    detailAlbum:{},
    detailCancion : {},
    listaCanciones: [],
    listaBandas: [],
    listaAlbums: [],
    listaIntegrantes: []
  },
  mutations: {
    modificarLogueded (state, payload) {
      state.logueded = payload.result
      state.userLogued = (payload.result ? payload.user : null)
    },
    modificarRegister (state, payload) {
      state.register = payload
    },
    modificarListaCanciones (state, payload) {
      state.listaCanciones = payload
    },
    modificarListaBandas (state, payload) {
      state.listaBandas = payload
    },
    modificarListaAlbums (state, payload) {
      state.listaAlbums = payload
    },
    modificarListaIntegrantes (state, payload) {
      state.listaIntegrantes = payload
    },
    modificarAlbum (state, payload) {
      state.detailAlbum = payload
    },
    modificarBanda (state, payload) {
      state.detailBanda = payload
    }
  },
  getters: {
    registrar: state => state.register,
    /*
    obtenerAlbum(state, payload){
      return album
    }
    */
  },
  actions: {
    async actionLogin (context, payload) {
      const url = 'https://localhost:44323/api/Login?User='.concat(payload.user).concat('&Pass=').concat(payload.pass)
      
      console.log('URL:' + url)

      await axios
        .get(url,
          {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
          }
        )
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
          context.commit('modificarLogueded', { result : result.data, user : payload.user})
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
    },
    async actionRegister (context, payload) {
      const url = 'https://localhost:44323/api/Registrar?User='.concat(payload.user).concat('&Pass=').concat(payload.pass)
      
      console.log('URL:' + url)

      let respuesta = false
      await axios
        .get(url)
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
          respuesta = result.data
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
          respuesta = false
        })

      context.commit('modificarRegister', respuesta)
    },
    async actionReseniaCancion (context, payload) {
      const url = 'https://localhost:44323/api/ResenaCancion'
      
      console.log('URL:' + url)
      if(payload.accion === 'new') {
        await axios
        .post(url,{ NombreUsuario: context.state.userLogued, CancionId : payload.cancionId, Valoracion: payload.resenia, Texto: payload.texto })
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
      }else if (payload.accion === 'edit') {
        await axios
        .put(url,{ NombreUsuario: context.state.userLogued, CancionId : payload.cancionId, Valoracion: payload.resenia, Texto: payload.texto })
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
      }else if (payload.accion === 'delete') {
        await axios
        .delete(url,{ data: { NombreUsuario: context.state.userLogued, CancionId : payload.cancionId, Valoracion: payload.resenia, Texto: payload.texto } })
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
      }

    },
    async actionReseniaBanda (context, payload) {
      const url = 'https://localhost:44323/api/ResenaBanda'
      
      console.log('URL:' + url)
      if(payload.accion === 'new') {
        await axios
        .post(url,{ NombreUsuario: context.state.userLogued, BandaId : payload.bandaId, Valoracion: payload.resenia, Texto: payload.texto })
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
      }else if (payload.accion === 'edit') {
        await axios
        .put(url,{ NombreUsuario: context.state.userLogued, BandaId : payload.bandaId, Valoracion: payload.resenia, Texto: payload.texto })
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
      }else if (payload.accion === 'delete') {
        await axios
        .delete(url,{ data: { NombreUsuario: context.state.userLogued, BandaId : payload.bandaId, Valoracion: payload.resenia, Texto: payload.texto } })
        .then((result) => {
          console.log('Respuesta del servicio:' + result.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
      }

    },
    async listarCanciones (context) {
      // Espero hasta que termine la invocacion del webService
      await axios
        .get('https://localhost:44323/api/Canciones?nombreUsuario='.concat(context.state.userLogued))
        .then((response) => {
          let result = response.data
          let listCanciones = []
          for (let i in result) {
            listCanciones.push({ id: result[i].Id,
                                nombre: result[i].Nombre,
                                anio: result[i].Anio,
                                duracion: result[i].Duracion,
                                cantante: result[i].Cantante.Nombre + ' ' + result[i].Cantante.Apellido,
                                genero: result[i].Genero.Nombre,
                                resenia: (result[i].Resenia == null ? 0 : result[i].Resenia.Valoracion),
                                reseniaTexto:(result[i].Resenia == null ? '' : result[i].Resenia.Texto)
                              })
          }

          //console.log(listCanciones)


          // Guardo la lista de canciones
          context.commit('modificarListaCanciones', listCanciones)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
    },
    async listarBandas (context) {
      await axios
        .get('https://localhost:44323/api/Bandas?nombreUsuario='.concat(context.state.userLogued))
        .then((response) => {
          let result = response.data
          let listBandas = []
          for (let i in result) {
            listBandas.push({ id: result[i].Id,
                          nombre: result[i].Nombre,
                          anioCreacion: result[i].AnioCreacion,
                          anioSeparacion: result[i].AnioSeparacion,
                          genero: result[i].Genero.Nombre,
                          resenia: (result[i].Resenia == null ? 0 : result[i].Resenia.Valoracion),
                          reseniaTexto:(result[i].Resenia == null ? '' : result[i].Resenia.Texto)
                        })
          }
          // Guardo la lista de canciones
          context.commit('modificarListaBandas', listBandas)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
    },
    async listarAlbums ({ commit }) {
      await axios
        .get('https://localhost:44323/api/Albums')
        .then((response) => {
          let result = response.data
          let listaAlbums = []
          for (let i in result) {
            //console.log(result[i])

            listaAlbums.push({ id: result[i].Id, nombre: result[i].Nombre, anioCreacion: result[i].AnioCreacion, banda: result[i].Banda.Nombre, genero: result[i].Genero.Nombre })
          }
          // Guardo la lista de canciones
          commit('modificarListaAlbums', listaAlbums)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
    },
    async obtenerServerAlbum ({ commit }, payload) {
      
      console.log('https://localhost:44323/api/Albums?idAlbum='+payload.idAlbum)
      
      await axios
        .get('https://localhost:44323/api/Albums?idAlbum='+payload.idAlbum)
        .then((response) => {
          console.log('Antes')

          commit('modificarAlbum', response.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
    },
    async obtenerServerBanda ({ commit }, payload) {
      
      console.log('https://localhost:44323/api/Bandas?idBanda='+payload.idBanda)
      
      await axios
        .get('https://localhost:44323/api/Bandas?idBanda='+payload.idBanda)
        .then((response) => {
          console.log('Antes')

          commit('modificarBanda', response.data)
        })
        .catch(error => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
    },
    async listarIntegrantes ({ commit }) {
      await axios
        .get('https://localhost:44323/api/Integrantes',{
          headers: {
            'Content-type': 'image/jpeg'
          }
        })
        .then((response) => {
          let result = response.data
          let listaIntegrantes = []
          for (let i in result) {
            //console.log(result[i])
            listaIntegrantes.push({
                                    id: result[i].id, nombre: result[i].Nombre,
                                    apellido: result[i].Apellido,
                                    nacimiento: moment(result[i].Nacimiento).format('DD/MM/YYYY'),
                                    foto: 'data:image/jpg;base64,'.concat(result[i].Foto) //Buffer.from(result[i].Foto, 'binary').toString('base64')
                                  })
          }
          // Guardo la lista de canciones
          commit('modificarListaIntegrantes', listaIntegrantes)
        })
        .catch( (error) => {
          console.log('>>>>Ocurrio un error dentro del axios:' + error)
        })
    }

  },
  modules: {
  }
})
