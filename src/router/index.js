/* eslint-disable space-before-blocks */
/* eslint-disable no-multiple-empty-lines */
/* eslint-disable no-unused-vars */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/welcome',
    name: 'Welcome',
    // Is lazy-loaded when the route is visited.
    component: function () {
      return import('../views/Welcome.vue')
    }
  },
  {
    path: '/bandas',
    name: 'Bandas',
    component: function (){
      return import('../views/Bandas.vue')
    }
  },
  {
    path: '/albums',
    name: 'Albums',
    component: function (){
      return import('../views/Albums.vue')
    }
  },
  {
    path: '/canciones',
    name: 'Canciones',
    component: function (){
      return import('../views/Canciones.vue')
    }
  },
  {
    path: '/integrantes',
    name: 'Integrantes',
    component: function (){
      return import('../views/Integrantes.vue')
    }
  }
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
